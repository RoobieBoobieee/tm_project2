/*$(document).ready(function() {
    $("#frmregister").submit(function(e){
        e.preventDefault();
        
        //add extra form elements
        var newElements = $("<input type=\"text\" name=\"name\" placeholder=\"Voornaam\"/><input type=\"text\" name=\"lastname\" placeholder=\"Achternaam\"/><input type=\"password\" name=\"password\" placeholder=\"Wachtwoord\"/> <input type=\"password\" name=\"password2\" placeholder=\"Bevestig Wachtwoord\"/> <div class=\"g-recaptcha\" data-sitekey=\"6LehbxkTAAAAAGm8nF-_S011NSxO_Aepw8xtfQk5\"></div>");
        //$('input#email').after(newElements);
        
        $("#frmregister").fadeOut(1000, function() { 
            //callback
            $(newElements).insertAfter("#email");
            $("#frmregister").fadeIn(1000);
        });


    });
});*/

$(document).ready(function(){
    $("#main-wrapper").onepage_scroll({

    });
});
$(document).ready(function() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
        $(".main").onepage_scroll({
            loop: false,                     // You can have the page loop back to the top/bottom when the user navigates at up/down on the first/last page.
        });
    }
});


function fullregisterform() {
        var newElements = $("<input type=\"text\" id=\"name\" name=\"name\" placeholder=\"Voornaam\" class=\"defaulttextbox\"/><input type=\"text\" id=\"lastname\" name=\"lastname\" placeholder=\"Achternaam\" class=\"defaulttextbox\"/><input type=\"password\" id=\"password\" name=\"password\" placeholder=\"Wachtwoord\" class=\"defaulttextbox\"/> <input type=\"password\" id=\"password2\" placeholder=\"Bevestig Wachtwoord\" class=\"defaulttextbox\"/>");

        $("#frmregister").fadeOut(1000, function() { 
            //callback
            $(newElements).insertAfter("#email");
            $("#frmregister").fadeIn(1000);
        });
        
    
        $("#submitbtn").attr('onclick','return validateform()');
        $("#frmregister").attr('action','post/register.php');
    
        return false;
}

function validateform() {
    var isValid = true;
    $('#frmregister input[type="text"], #frmregister input[type="email"], #frmregister input[type="password"]').each(function() {
        
        if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).switchClass("defaulttextbox", "ui-state-error");
        } else {
                $(this).switchClass("ui-state-error", "defaulttextbox");
        }
    });
    
    //reset errorbox
    var show = false;
    if ($("#errormsg").hasClass("show")) {
        $("#errormsg").switchClass("show", "hide", 0);
    }
    
    $('#form-error').html("<p id=\"errormsgbody\"></p>");
    
    if (!isValid) {
        $('#errormsgbody').append("<p class=\"fat\">Error: </p>Niet alle velden zijn ingevuld.<br />")
        show = true;
        $("#errormsg").switchClass("hide", "show");
    }
    
    //check passwords
    if ($("#password").val() != $("#password2").val()) {
        $('#errormsgbody').append("<p class=\"fat\">Error: </p>Wachtwoorden zijn niet gelijk.<br />")
        show = true;
    }
    
    //check captcha
    if(grecaptcha.getResponse().length === 0)
    {
        $('#errormsgbody').append("<p class=\"fat\">Error: </p>reCAPTCHA niet gechecked.<br />")
        show = true;
    }
    
    if (show) {
        $("#errormsg").switchClass("hide", "show");
        return false;
    } else {
        return true;
    }
    return false;
}