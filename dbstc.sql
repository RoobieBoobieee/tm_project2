-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 23 mei 2016 om 22:18
-- Serverversie: 5.6.26
-- PHP-versie: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbstc`
--
CREATE DATABASE IF NOT EXISTS `dbstc` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbstc`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_authcodes`
--

CREATE TABLE IF NOT EXISTS `t_authcodes` (
  `user_id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `t_authcodes`
--

INSERT INTO `t_authcodes` (`user_id`, `code`) VALUES
(1, 'fq4XdGALHtaDitRhLEhCcJIjncTRzhD-J0eJcGxnDGPEgU3J');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_classes`
--

CREATE TABLE IF NOT EXISTS `t_classes` (
  `id` varchar(32) NOT NULL,
  `startTime` time NOT NULL,
  `date` date NOT NULL,
  `academicYear` year(4) NOT NULL,
  `endTime` time NOT NULL,
  `weekDay` tinyint(4) NOT NULL,
  `ects` text NOT NULL,
  `shortDescription` tinytext NOT NULL,
  `longDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `t_classes`
--

INSERT INTO `t_classes` (`id`, `startTime`, `date`, `academicYear`, `endTime`, `weekDay`, `ects`, `shortDescription`, `longDescription`) VALUES
('54052A4B74450950E1008000863A0E2B', '09:00:00', '2014-10-23', 2014, '11:00:00', 3, 'P0M116a', 'Statistiek vr psychologen, dl1:practicum', 'Statistics for Psychologists, Part 1: Practicum');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_lessen`
--

CREATE TABLE IF NOT EXISTS `t_lessen` (
  `id` int(11) NOT NULL,
  `class_name` varchar(64) NOT NULL,
  `class_location` varchar(11) NOT NULL,
  `start_time` time NOT NULL,
  `stop_time` time NOT NULL,
  `weekday` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `t_lessen`
--

INSERT INTO `t_lessen` (`id`, `class_name`, `class_location`, `start_time`, `stop_time`, `weekday`) VALUES
(1, 'Domotica: theorie', 'F110', '12:15:00', '13:15:00', 1),
(2, 'Advanced object oriented programming labo', 'A116', '14:15:00', '16:45:00', 1),
(3, 'Python labo', 'A004', '13:15:00', '14:45:00', 2),
(4, 'Advanced object oriented programming theorie', 'A118', '14:45:00', '15:45:00', 2),
(5, 'Python: theorie', 'K108', '08:15:00', '10:15:00', 3),
(6, 'Computernetwerken 2', 'A116', '10:15:00', '12:15:00', 3),
(7, 'Installatietechnieken: theorie', 'K104', '13:15:00', '14:45:00', 3),
(8, 'Project 2 SICT labo', 'A116', '14:45:00', '18:15:00', 3),
(9, 'Python: theorie', 'A113', '08:15:00', '10:15:00', 4),
(10, 'Computerinfrastructuur', 'A014', '09:45:00', '11:15:00', 5),
(13, 'Domotica: labo', 'D016', '12:15:00', '14:15:00', 5),
(14, 'Advanced object oriented programming theorie', 'A118', '15:15:00', '16:45:00', 5);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_les_student`
--

CREATE TABLE IF NOT EXISTS `t_les_student` (
  `user_id` int(11) NOT NULL,
  `les_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `t_les_student`
--

INSERT INTO `t_les_student` (`user_id`, `les_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 13),
(1, 14),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(3, 9),
(3, 10),
(3, 13),
(3, 14);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_location`
--

CREATE TABLE IF NOT EXISTS `t_location` (
  `id` int(11) NOT NULL,
  `longDescription` text NOT NULL,
  `roomNumber` text NOT NULL,
  `roomName` text NOT NULL,
  `coordY` decimal(10,0) NOT NULL,
  `coordX` decimal(10,0) NOT NULL,
  `buildingName` text NOT NULL,
  `city` text NOT NULL,
  `street` text NOT NULL,
  `mnemonic` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_schedule`
--

CREATE TABLE IF NOT EXISTS `t_schedule` (
  `class_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_settings`
--

CREATE TABLE IF NOT EXISTS `t_settings` (
  `user_id` int(11) NOT NULL,
  `car` tinyint(1) NOT NULL DEFAULT '0',
  `transit` tinyint(1) NOT NULL DEFAULT '1',
  `train` tinyint(1) NOT NULL DEFAULT '1',
  `tram` tinyint(1) NOT NULL DEFAULT '1',
  `bus` tinyint(1) NOT NULL DEFAULT '1',
  `address_text` text NOT NULL,
  `address_google_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `t_settings`
--

INSERT INTO `t_settings` (`user_id`, `car`, `transit`, `train`, `tram`, `bus`, `address_text`, `address_google_id`) VALUES
(1, 0, 1, 1, 1, 1, 'Mechelen, BelgiÃ«', 'EhtCaXN0IDI1LCAyNTAwIExpZXIsIEJlbGdpw6s'),
(2, 0, 1, 0, 0, 1, 'Bist 25, Lier, Belgium', 'aff3cd3fc6100ccfd80d72707122149727037c5c'),
(3, 0, 1, 0, 0, 0, 'Rysheuvelsstraat, Antwerp, Belgium', 'ChIJ3_UgVjL3w0cR9dDWJY9g0Rs');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_teacher`
--

CREATE TABLE IF NOT EXISTS `t_teacher` (
  `id` varchar(8) NOT NULL,
  `name` text NOT NULL,
  `personnelNumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `t_teacher`
--

INSERT INTO `t_teacher` (`id`, `name`, `personnelNumber`) VALUES
('00014428', 'Prof. dr. Van Mechelen Iven', 'U0014428');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_users`
--

CREATE TABLE IF NOT EXISTS `t_users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `t_users`
--

INSERT INTO `t_users` (`id`, `email`, `password`, `name`, `lastname`) VALUES
(1, 'Robvankeilegom@hotmail.com', '$2y$10$.vTAx1c1eH8p1MnFbH.up.AjF6jm5fnXrK8T2DcJvqeD1bz4wUqTe', 'Rob', 'Van Keilegom'),
(2, 'chanske@hotmail.com', '$2y$10$DPuxN4A06xGai5iYofSReeXreXPXyJQ8HWYsTq66l8YFkuRcm84JS', 'Channi', 'Ceulemans'),
(3, 'quick_fire@hotmail.com', '$2y$10$DD/TCWaftjhV2.kLLynhJevNYP119M81lKr9QsQZtjbU3sR8VH53K', 'Marlon', 'Stoops'),
(4, 'test@test.com', '$2y$10$Xy78s0wkUQQfrTFdKOjYr.DVrJ/iGpAtcCbkAQG0GM2N8HsE7GpU6', 'Test', 'Account');

-- --------------------------------------------------------

--
-- Stand-in structuur voor view `v_les_student`
--
CREATE TABLE IF NOT EXISTS `v_les_student` (
`user_id` int(11)
,`class_name` varchar(64)
,`class_location` varchar(11)
,`start_time` time
,`stop_time` time
,`weekday` int(11)
);

-- --------------------------------------------------------

--
-- Structuur voor de view `v_les_student`
--
DROP TABLE IF EXISTS `v_les_student`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_les_student` AS select `t_les_student`.`user_id` AS `user_id`,`t_lessen`.`class_name` AS `class_name`,`t_lessen`.`class_location` AS `class_location`,`t_lessen`.`start_time` AS `start_time`,`t_lessen`.`stop_time` AS `stop_time`,`t_lessen`.`weekday` AS `weekday` from (`t_les_student` join `t_lessen` on((`t_les_student`.`les_id` = `t_lessen`.`id`)));

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `t_authcodes`
--
ALTER TABLE `t_authcodes`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexen voor tabel `t_classes`
--
ALTER TABLE `t_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `t_lessen`
--
ALTER TABLE `t_lessen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexen voor tabel `t_settings`
--
ALTER TABLE `t_settings`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexen voor tabel `t_teacher`
--
ALTER TABLE `t_teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `t_lessen`
--
ALTER TABLE `t_lessen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT voor een tabel `t_users`
--
ALTER TABLE `t_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
