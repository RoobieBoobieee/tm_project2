<html>
<head>
    <?php 
        include ("inc/header.php");
        if (!isset($_SESSION['ID'])) {
            header('Location: index.php');
        }
    ?>

<link rel="stylesheet" type="text/css" href="css/dasboard.css">
    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANUu89aev6NFYdrn48Se1pmrw3_cwIW6A&libraries=places"></script>
<script src='js/moment.min.js'></script>
<script src='js/fullcalendar.js'></script>
<script src='js/fullcalendar-nl.js'></script>
<script src="js/dashboard.js"></script> 

<script type="text/javascript">
$(document).ready(function() {

    $('#calendar').fullCalendar({
        header: false,
        theme: true,
        defaultView: 'agendaDay',
        allDaySlot: false,
        height: $(window).height()*0.858,
        lang: '',
        businessHours: {
        start: '08:00', 
        end: '19:00', 
        },
        defaultDate: 
        <?php
            $date = date('N');
            $datetime = new DateTime();
            if (date('H') > 19) {
                $date += 1;
                $datetime->modify('+1 day');
            } 
            echo '\'' . $datetime->format('Y-m-d\TH:i:s') . '\'';         
        ?>,
        events: [
            /*
            EXAMPLE
            {
                title  : 'Installation Techniques: Theorie',
                start  : '2016-03-18T13:15:00',
                end    : '2016-03-18T14:45:00'
            },
            {
                title  : 'Installation Techniques: Theorie',
                start  : '2016-03-18T13:15:00',
                end    : '2016-03-18T14:45:00'
            },
            {
                title  : 'Project 2: Labo',
                start  : '2016-03-18T14:45:00',
                end    : '2016-03-18T18:15:00'
            },
             */   
            

            <?php
                $time_first_lesson = 0;
        
        
                $query = "SELECT * FROM `v_les_student` WHERE `weekday` = " . $date . " AND `user_id` = " . $_SESSION['ID'];
                
                $result = mysqli_query($conn, $query);
                $first = true;
                while($row = $result->fetch_assoc()) {        
                    if (!$first) {
                        echo ",";
                       
                    }
                    
                    // MOET NOG GECONTROLEERD WORDEN
                    if  (strtotime($time_first_lesson) < time()) {
                        $time_first_lesson = $row["start_time"];
                    }
                    echo ('{ ');
                    echo ('title : \'' . $row["class_name"] . '\', ');
                    echo ('start : \'' . $datetime->format('Y-m-d') . "T" . $row["start_time"] . '\', ');
                    echo ('end : \'' . $datetime->format('Y-m-d') . "T" . $row["stop_time"] . '\'');
                    
                    echo (' }');
                        
                    $first = false;
                }

            ?>
        ]
    });

});

</script>   
<title>Home</title>    
</head>
<body>
<main>
    <div id="calendar"></div>
    <div>
    
        <?php
        
        $time_first_lesson = strtotime($time_first_lesson);
        
        
        if ($time_first_lesson < time()) {
            $query = "SELECT * FROM `v_les_student` WHERE `weekday` = " . (date('N') + 1) . " AND `user_id` = " . $_SESSION['ID'] . " LIMIT 1";
            $result = mysqli_query($conn, $query);
            $row = $result->fetch_assoc(); 
            $time_first_lesson = new DateTime($row["start_time"]);
            $time_first_lesson->modify('+1 day');
            $time_first_lesson = $time_first_lesson->format('U'); 
            //$time_first_lesson = strtotime($time_first_lesson);

            
        }
                    
        $query = "SELECT * FROM `t_settings` WHERE `user_id` = " . $_SESSION['ID'];
        $result = mysqli_query($conn, $query);
        $transport = $result->fetch_assoc(); 
        
        $start = $transport["address_google_id"];
        
        if ($start == "") {
            $start = "EitMLnZhbiBDYW1wZW5ob3V0c3RyIDQwLCAyODcwIFB1dXJzLCBCZWxnacOr";
        }
        
        $end = "ChIJJ4tRNeHkw0cRA9KLZnpZDyY";
        
        $transit_mode = "";
        
        if ($transport["car"]) {
            $mode = 'driving'; 
        } else {
            $mode = 'transit'; 
            if ($transport["train"]) {
                $transit_mode .= "train|";
            } 
            if ($transport["tram"]) {
                $transit_mode .= "tram|subway|";
            }
            if ($transport["bus"]) {
                $transit_mode .= "bus|";
            }
            if ($transit_mode != "") {
                $transit_mode = substr($transit_mode, 0, -1); 
            }
        }
        
        $arrival_time = $time_first_lesson; // INT --- number of seconds since the Unix Epoch (January 1 1970 00:00:00 GMT).
        
        
        $url = "";
        $url .= 'https://maps.googleapis.com/maps/api/directions/json?';
        $url .= 'origin=place_id:' . $start;
        $url .= '&destination=place_id:' . $end;
        $url .= '&mode=' . $mode;
        $url .= '&transit_mode=' . $transit_mode;
        $url .= '&arrival_time=' . $arrival_time;
        $url .= '&language=nl';
        $url .= '&key=' . $google_api_key;
        $result = file_get_contents($url);
        $json = json_decode($result, true);
        
        
        if (!$transport["car"]) {
            echo "<p class=\"fat\">";
            echo "Vertrek tijd: " . $json["routes"][0]["legs"][0]["departure_time"]["text"]. "<br />";          
            echo "</p>";
            echo "<hr />";
        }
        
        foreach ($json["routes"][0]["legs"][0]["steps"] as $value) {    
            echo "<p class=\"fat\">" . $value["html_instructions"] . "</p>";
            if ($value["travel_mode"] == "TRANSIT") {
                
                echo " " . $value["transit_details"]["departure_time"]["text"] . "<br />";
                echo "<p class=\"half\">Halte(s): " . $value["transit_details"]["num_stops"] . "</p>";
                echo "Eindhalte: " . $value["transit_details"]["arrival_stop"]["name"];

            }
            echo "<br />";
            echo "<p class=\"half\">Tijd:" . $value["duration"]["text"] . "</p>";
            echo "Afstand: " . $value["distance"]["text"];

            echo "<hr />";
        }

        if (!$transport["car"]) {
            echo "<p class=\"fat\">";
            echo "Aankomst tijd: " . $json["routes"][0]["legs"][0]["arrival_time"]["text"]. "<br />";
            echo "</p>";
            echo "<hr />";
        }

        
        echo "Totale tijd: " . $json["routes"][0]["legs"][0]["duration"]["text"] . "<br />";
        echo "Totale afstand: " . $json["routes"][0]["legs"][0]["distance"]["text"];


        ?>


        
    </div>
    <div id="settings">
        <p class="fat">Instellingen</p>
        <form action="post/savesettings.php" method="post">
            <div id="vervoer">
                <input type="radio" id="auto" name="vervoer" value="car"
                <?php
                    if ($transport["car"]) {
                        echo " checked";
                    }     
                ?>
                >
                <label for="auto">Auto</label>
                <input type="radio" id="openbaar" name="vervoer" value="transit"
                <?php
                    if (!$transport["car"]) {
                        echo " checked";
                    }     
                ?>
                >
                <label for="openbaar">Openbaar Vervoer</label>
            </div>        
            <div id="openbaarvervoer">
                <input type="checkbox" id="trein" name="trein"
                <?php
                    if ($transport["train"]) {
                        echo " checked";
                    }     
                ?>
                />
                <label for="trein">Trein</label>
                <input type="checkbox" id="tram" name="tram"
                <?php
                    if ($transport["tram"]) {
                        echo " checked";
                    }     
                ?>
                />
                <label for="tram">Tram</label>
                <input type="checkbox" id="bus" name="bus"
                <?php
                    if ($transport["bus"] ) {
                        echo " checked";
                    }     
                ?>
                />
                <label for="bus">Bus</label>



            </div>
            <hr />
            <div id="address">
                <label for="address">Vertrekplaats:</label>
                <input id="address_text" name="address_text" placeholder="Enter your address" onFocus="geolocate()" type="text" value="<?php echo $transport["address_text"]; ?>" />
                <input type="hidden" id="placeid" name="placeid" value="<?php echo $transport["address_google_id"]; ?>"/>

                <hr class="clear" />
            </div>

            <div id="save">
                <input type="submit" value="Opslaan"/>
            </div>
        </form>
    </div>

</main>
</body>
</html>